import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.example.FizzBuzz;


public class FizzBuzzTest {
    @Test
    public void testFizzMultipleOfThree(){
        assertEquals("Fizz", FizzBuzz.fizzBuzz(6));
    }

    @Test
    public void testFizzContainsThree(){
        assertEquals("Fizz", FizzBuzz.fizzBuzz(31));
    }

    @Test
    public void testBuzzMultipleOfFive(){
        assertEquals("Buzz", FizzBuzz.fizzBuzz(10));
    }

    @Test
    public void testBuzzContainsFive(){
        assertEquals("Buzz", FizzBuzz.fizzBuzz(52));
    }

    @Test
    public void testFizzBuzzMultipleOfThreeMultipleOfFive(){
        assertEquals("FizzBuzz", FizzBuzz.fizzBuzz(15));
    }

    @Test
    public void testFizzBuzzMultipleOfThreeContainsFive(){
        assertEquals("FizzBuzz", FizzBuzz.fizzBuzz(51));
    }

    @Test
    public void testFizzBuzzContainsThreeMultipleOfFive(){
        assertEquals("FizzBuzz", FizzBuzz.fizzBuzz(35));
    }

    @Test
    public void testFizzBuzzContainsThreeContainsFive(){
        assertEquals("FizzBuzz", FizzBuzz.fizzBuzz(53));
    }
}
