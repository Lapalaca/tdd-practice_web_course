package org.example;

public class FizzBuzz{

    public static String fizzBuzz(int num){
        String res = "";
        boolean fizz = false;
        boolean buzz = false;

        if( (num % 3 ==0) || Integer.toString(num).contains("3")){
            fizz = true;
        }

        if( (num % 5 ==0) || Integer.toString(num).contains("5")){
            buzz = true;
        }

        if(fizz){
            res += "Fizz";
        }
        if(buzz){
            res += "Buzz";
        }


        return res;
    }

}
